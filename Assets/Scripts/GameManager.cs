﻿using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    private const int SPAWNTIME         = 1;

    private const int MINENEMYSIZE      = 2;
    private const int MAXENEMYSIZE      = 4;

    private const int SPAWNFIELDSIZE    = 7;

    private int nextUpdate = 0;
    private int downCounter = 3;
    private bool awakeTheEvoker = false;

    public Text         numberText;
    public GameObject   enemyPrefab;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        // InitializeGame();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;

        SceneManager.UnloadSceneAsync   ("MenuScene");
        SceneManager.LoadScene          ("GameScene", LoadSceneMode.Single);
    }

    private void InitializeGame()
    {
        numberText = (Text)FindObjectsOfType(typeof(Text))[0];
        // enemyPrefab = FindObjectsOfTypeIncludingAssets(Enemy);
        enemyPrefab = PrefabUtility.LoadPrefabContents("Assets/Prefabs/Enemy.prefab");
        // PrefabUtility.GetPrefabParent(gameObject)

        this.StartCoroutine(Countdown());
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        if (arg0.name == "GameScene")
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByName("GameScene"));

            // SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetSceneByName("GameScene"));
            InitializeGame();



        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    IEnumerator Countdown()
    {
        while (downCounter >= 0)
        {
            if (downCounter <= 0)
            {
                numberText.enabled = false;
                awakeTheEvoker = true;
                break;
            }

            numberText.text = downCounter + "";
            downCounter--;
            yield return new WaitForSecondsRealtime(1);
        }
    }

    private void FixedUpdate()
    {
        if (awakeTheEvoker)
        {
            if (Time.time >= nextUpdate)
            {
                nextUpdate = Mathf.FloorToInt(Time.time) + SPAWNTIME;
                TimedSpawn();
            }
        }
    }

    void TimedSpawn()
    {
        GameObject spawnedEnemy = enemyPrefab;

        int randomSize = (int)(Random.value * MAXENEMYSIZE);

        if (randomSize < MINENEMYSIZE)
            randomSize = MINENEMYSIZE;

        spawnedEnemy.transform.localScale = (new Vector3(1, 1, 1) * randomSize);

        int randomizeEnemyX = (int)(Random.value * SPAWNFIELDSIZE);
        if (Random.value < 0.5f)
            randomizeEnemyX = randomizeEnemyX * -1;

        int randomizeEnemyY = (int)(Random.value * SPAWNFIELDSIZE);
        if (Random.value < 0.5f)
            randomizeEnemyY = randomizeEnemyY * -1;

        Instantiate(enemyPrefab, new Vector3(randomizeEnemyX, 20, randomizeEnemyY), new Quaternion());
    }
}
