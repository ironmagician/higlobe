﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyPlayerController : MonoBehaviour
{
    private const bool DEVMODE  = true;
    private const int TIMEHANDLER = 1;
    private const float SPEED   = 3.33f;
    private const float UP_STEP = 1.20f;
    private const float UP_MAX  = 6f;

    private Rigidbody body;

    private float   upFloat             = 0f;
    private bool    goUp                = false;
    private bool    spaceBeingPressed   = false;

    private Vector3 initialPosition;

    // Start is called before the first frame update
    void Start()
    {
        this.body               = GetComponent<Rigidbody>();
        this.initialPosition    = this.body.position;

        this.StartCoroutine(secLoop());
    }

    IEnumerator secLoop()
    {
        while (true)
        {
         
            yield return new WaitForSecondsRealtime(TIMEHANDLER);
        }
    }
    // Update is called once per frame
    void Update()
    {
        float moveHorizontal    = Input.GetAxis("Horizontal");
        float moveVertical      = Input.GetAxis("Vertical");

        if (Input.GetKeyDown(KeyCode.R))
        {
            this.GameOver(false);
            // body.
            return;
        }

        if(spaceBeingPressed && Input.GetKeyUp("space"))
            spaceBeingPressed = false;

        if (Input.GetKeyDown("space") && !goUp && !spaceBeingPressed && body.position.y <= 1)
        {
            spaceBeingPressed = true;
            goUp = true;
        }
        else if (upFloat <= UP_MAX && goUp)
        {
            upFloat += UP_STEP;
        }
        else if (upFloat > UP_MAX && goUp)
        {
            goUp = false;
        }
        else if (upFloat > 0 && !goUp)
        {
            upFloat -= UP_STEP * 3;

            if (upFloat <= 0)
                upFloat = 0;
        }

        // is inside varialbe spotlight place
        if (this.body.position.magnitude > 3 && this.body.position.magnitude < 8)
            this.BroadcastMessage("SetLightIntensity", this.body.position.magnitude);


        if (get2Dmagnitude(body.velocity) < 4)
        {
            Vector3 movement = new Vector3(moveHorizontal, upFloat, moveVertical);
            body.AddForce(movement * SPEED);
        }



    }

    private void FixedUpdate()
    {
        Debug.Log(CalculateAngle());
    }

    private float get2Dmagnitude (Vector3 velocity)
    {
        // Ignore up and down force
        velocity.y = 0;
        return velocity.magnitude;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("BadBlock"))
        {
            this.GameOver(false);
        }
    }

    private void GameOver(bool won)
    {
        SceneManager.LoadScene("MenuScene");
    }

    private float CalculateAngle()
    {
        float angle = Vector3.Angle(new Vector3(0, 0, 0).normalized, this.body.position.normalized);
        return angle;
    }
}
