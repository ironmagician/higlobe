﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    public bool hasCollidedWithFloor    = false;
    public bool isCoin                  = false;
    private Renderer rend;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(!hasCollidedWithFloor && collision.gameObject.CompareTag("Floor"))
        {
            hasCollidedWithFloor = true;
            StartCoroutine(EnemyToCoinTimer());
        }
        else if( hasCollidedWithFloor && gameObject.CompareTag("Coin") && collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator EnemyToCoinTimer()
    {
        while (true)
        {
            if(isCoin)
            {
                Material newMat = Resources.Load("CoinMaterial", typeof(Material)) as Material;
                rend = GetComponent<Renderer>();

                gameObject.tag = "Coin";
                gameObject.transform.localScale = new Vector3(1,1,1) * 0.4f;
                rend.material = newMat;

                break;
            } else
            {
                isCoin = true;
            }
            yield return new WaitForSecondsRealtime(10);
        }
    }
}
