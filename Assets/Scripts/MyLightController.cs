﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyLightController : MonoBehaviour
{
    private const float MIN_DISTANCE = 3;
    private const float MAX_DISTANCE = 8;

    float startingIntensity;
    Light lantern;

    // Start is called before the first frame update
    void Start()
    {
        lantern = this.GetComponent<Light>();
        startingIntensity = lantern.intensity;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetLightIntensity(float distance)
    {
        lantern.intensity = (MIN_DISTANCE * startingIntensity) / distance;
    }
}
